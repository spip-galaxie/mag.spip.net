<?php

// This is a SPIP language file -- Ceci est un fichier langue de SPIP
// pour les squelettes de SPIP Mag'

$GLOBALS[$GLOBALS['idx_lang']] = array (

// A
'a_propos' => '&Agrave; propos de ce site',
'a_retenir' => '&Agrave; retenir',
'abo_liste' => 'Pour participer ou vous tenir au courant, abonnez-vous &agrave; la mailing list de SPIP Mag&#8217;',
'agenda' => 'Agenda',
'aller_contenu' => 'Aller au contenu',
'aller_forum' => 'Aller au forum',
'aller_menu' => 'Aller au menu',
'aller_recherche' => 'Aller &agrave; la recherche',
'aucun_article' => 'Aucun article ne correspond &agrave; votre requ&ecirc;te.',

// C
'calendrier' => 'Calendrier',
'contact' => 'Contact',

// D
'dans_rubrique' => 'Dans cette rubrique',
'date_public_detail' => 'Date de publication de cette page',
'date_update_detail' => 'Date de la derni&egrave;re modification de cette page',
'documents_joints' => 'Documents joints',

// E
'epingle' => 'Epingl&eacute;',
'et' => 'et',
'exemple' => 'Exemple pour la contrib',

// F
'recherche_mot' => 'Faire une recherche parmi les articles li&eacute;s &agrave; ce mot-cl&eacute;',
'fil_atom' => 'Fil Atom',
'fil_rss' => 'Fil RSS',
'form_recherche' => 'Formulaire de recherche',

// I
'imprimer' => 'Imprimer cette page',
'index_auteurs' => 'Index des auteurs',

// K
'kwadneuf' => 'Quoi de neuf&nbsp;?',

// L
'lastmod' => 'Derni&egrave;re mise &agrave; jour du site',
'liens' => 'Liens',
'liens_privilegies' => 'Liens privil&eacute;gi&eacute;s',

// M
'mag' => 'Mag&#8217;',
'mailing_list' => 'Mailing list',
'mentions_legales' => 'Mentions l&eacute;gales',
'mis_a_jour_le' => 'Mis &agrave; jour le',

// N
'nombre_visiteurs' => 'Nombre de visiteurs',

// O
'ordre_antichronologique' => 'Ordre antichronologique',
'ordre_chronologique' => 'Ordre chronologique',
'ordre_inverse' => 'ordre inverse',
'ordre_normal' => 'ordre normal',

// P
'par_date' => 'par date',
'par_pertinence' => 'par pertinence',
'participer' => 'Participer au site',
'popularite_detail' => 'Nombre de visiteurs par jour',
'public' => 'Date de publication',
'publie_le' => 'Publi&eacute; le',

// Q

// R
'recherche' => 'Recherche',
'retour_accueil' => 'Retour &agrave; la page d\'accueil',

// S
'site_heberge_par' => 'Site h&eacute;berg&eacute; par',
'sondage' => 'Sondage',
'sous_rubriques' => 'Sous-rubriques',
'spip' => 'SPIP',
'squelettes' => 'Les squelettes de ce site sont librement consultables',
'sur_spipmag' => 'Sur SPIP Mag&#8217;',

// T
'titre_forum' => 'Les r&eacute;actions re&ccedil;ues &agrave; cet article',
'traductions' => 'Traductions de cet article&nbsp;',
'trier_date' => 'Trier par date',
'trier_par' => 'Trier par&nbsp;',
'trier_pertinence' => 'Trier par pertinence',
'tris_divers' => 'Tris divers',

// U
'update' => 'Date de mise &agrave; jour',

// V
'visiteurs' => 'visiteurs',
'visiteurs_detail' => 'Nombre total de visites sur cette page',
'visiteurs_par_jour' => 'visiteurs par jour',
'votre_requete' => 'Votre requ&ecirc;te',

// X
'xhtml_valide' => 'Xhtml valide'




);

?>
